# Dutch translation for the configuration of mdadm
#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
# Frans Pop <aragorn@tiscali.nl>, 2005, 2006.
# Frans Pop <elendil@planet.nl>, 2008.
# Maarten <Maarten@posteo.de>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: mdadm 4.1-3\n"
"Report-Msgid-Bugs-To: mdadm@packages.debian.org\n"
"POT-Creation-Date: 2019-02-09 08:48+0100\n"
"PO-Revision-Date: 2019-09-19 22:51+0200\n"
"Last-Translator: Maarten <Maarten@posteo.de>\n"
"Language-Team: Dutch <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.11\n"

# Zie voor mijn notatie het artikel over 'disk array' in de Engelse Wikipedia. Zie ook het artikel over 'Disk-Array' in de Duitse Wikipedia. De Duitsers vertalen 'disk' dus niet naar 'Festplatte'.
#. Type: boolean
#. Description
#: ../mdadm.templates:2001
msgid "Should mdadm run monthly redundancy checks of the MD arrays?"
msgstr ""
"Moet mdadm maandelijkse redundatiecontroles van de disk-arrays uitvoeren?"

#. Type: boolean
#. Description
#: ../mdadm.templates:2001
msgid ""
"If the kernel supports it (versions greater than 2.6.14), mdadm can "
"periodically check the redundancy of MD arrays (RAIDs). This may be a "
"resource-intensive process, depending on the local setup, but it could help "
"prevent rare cases of data loss. Note that this is a read-only check unless "
"errors are found; if errors are found, mdadm will try to correct them, which "
"may result in write access to the media."
msgstr ""
"Als uw kernel dit ondersteunt (de versie is groter dan 2.6.14), dan kan "
"mdadm periodiek de redundantie van uw disk-arrays controleren. Afhankelijk "
"van de configuratie kan dit een zware systeembelasting teweegbrengen, maar "
"het zou ook zeldzame gevallen van gegevensverlies kunnen voorkomen. De "
"controle gebruikt alleen leesopdrachten, tenzij fouten worden gevonden. Als "
"fouten worden gevonden, dan zal mdadm deze proberen te herstellen, hetgeen "
"schrijfopdrachten tot gevolg kan hebben."

#. Type: boolean
#. Description
#: ../mdadm.templates:2001
msgid ""
"The default, if turned on, is to check on the first Sunday of every month at "
"01:06."
msgstr ""
"Als de controle is geactiveerd, dan wordt deze standaard uitgevoerd op elke "
"eerste zondag van de maand om 01:06 uur 's nachts."

#. Type: boolean
#. Description
#: ../mdadm.templates:3001
msgid "Should mdadm check once a day for degraded arrays?"
msgstr "Moet mdadm eens per dag controleren op in verval geraakte disk-arrays?"

# such events -> those events
#. Type: boolean
#. Description
#: ../mdadm.templates:3001
msgid ""
"mdadm can check once a day for degraded arrays and missing spares to ensure "
"that such events don't go unnoticed."
msgstr ""
"mdadm kan eens per dag controleren op in verval geraakte disk-arrays en op "
"ontbrekende reserveschijven om er zeker van te zijn dat die toestanden niet "
"onopgemerkt blijven."

#. Type: boolean
#. Description
#: ../mdadm.templates:4001
msgid "Do you want to start the MD monitoring daemon?"
msgstr "Wilt u de achtergronddienst voor de disk-array-monitor starten?"

#. Type: boolean
#. Description
#: ../mdadm.templates:4001
msgid ""
"The MD (RAID) monitor daemon sends email notifications in response to "
"important MD events (such as a disk failure)."
msgstr ""
"De achtergronddienst voor de disk-array-monitor stuurt per e-mail berichten "
"bij belangrijke gebeurtenissen (zoals de uitval van een harde schijf)."

#. Type: boolean
#. Description
#: ../mdadm.templates:4001
msgid "Enabling this option is recommended."
msgstr "Gebruik van deze optie wordt aanbevolen."

#. Type: string
#. Description
#: ../mdadm.templates:5001
msgid "Recipient for email notifications:"
msgstr "Adres voor e-mailberichten:"

#. Type: string
#. Description
#: ../mdadm.templates:5001
msgid ""
"Please enter the email address of the user who should get the email "
"notifications for important MD events."
msgstr ""
"Voer het e-mailadres in van de gebruiker die de e-mailberichten over "
"belangrijke gebeurtenissen met betrekking tot de disk-arrays dient te "
"ontvangen."
